package com.blank.main.ui.main.developer

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.blank.core.base.BaseFragment
import com.blank.main.BR
import com.blank.main.R
import com.blank.main.databinding.FragmentDevBinding
import com.blank.widget.utils.observe
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class DevFragment : BaseFragment<FragmentDevBinding, DevViewModel>() {

    override fun layoutId(): Int = R.layout.fragment_dev
    override fun bindingVariable(): Int = BR.vmdev
    override val viewModel: DevViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.btnAdd?.setOnClickListener {

        }

        binding?.btnShow?.setOnClickListener {
            viewModel.getForm()
        }

        observe(viewModel.form) {
            Timber.e(it.toString())
        }

        observe(viewModel.loading) {
            binding?.pbResponses?.isVisible = it
        }

        observe(viewModel.statusAdd) {
            val msg = if (it) "Sukses" else "Gagal"
            Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
        }
    }
}