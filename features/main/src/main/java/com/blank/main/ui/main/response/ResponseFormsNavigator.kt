package com.blank.main.ui.main.response

import com.blank.core.base.BaseView
import com.blank.models.local.entity.ResponseEntity

interface ResponseFormsNavigator : BaseView {
    fun resultResponseForm(responses: MutableList<ResponseEntity>)
}