package com.blank.main.ui.main.developer

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.blank.core.base.BaseViewModel
import com.blank.core.data.DataManager
import com.blank.models.local.entity.ElementEntity
import com.blank.models.local.entity.FormEntity
import com.blank.models.local.entity.FormRelation
import com.blank.models.local.entity.ResponseEntity
import timber.log.Timber

class DevViewModel @ViewModelInject constructor(private val dataManager: DataManager) :
    BaseViewModel() {

    val statusAdd = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>(false)
    val form = MutableLiveData<MutableList<FormRelation>>()

    fun addForm(
        formEntity: FormEntity,
        responseEntity: ResponseEntity,
        elementEntity: ElementEntity
    ) {
        dataManager.insertForm(formEntity)
            .doOnSubscribe {
                loading.value = true
            }
            .doOnError {
                statusAdd.value = false
            }
            .doOnTerminate {
                loading.value = false
            }
            .flatMap {
                dataManager.insertResponse(responseEntity)
            }
            .flatMap {
                dataManager.insertElement(elementEntity)
            }
            .subscribe(statusAdd::setValue, Timber::e)
            .autoDispose()
    }

    fun getForm() {
        dataManager.getAllForm()
            .doOnSubscribe {
                loading.value = true
            }
            .doOnTerminate {
                loading.value = false
            }
            .subscribe(form::setValue, Timber::e)
    }
}