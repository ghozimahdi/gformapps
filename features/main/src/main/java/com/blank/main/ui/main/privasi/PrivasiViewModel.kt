package com.blank.main.ui.main.privasi

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.blank.core.base.BaseViewModel
import com.blank.core.data.DataManager
import com.blank.models.local.entity.FormRelation
import timber.log.Timber

class PrivasiViewModel @ViewModelInject constructor(private val dataManager: DataManager) :
    BaseViewModel() {

    val listForms = MutableLiveData<MutableList<FormRelation>>()

    fun getFormsRelationLive() = dataManager.getAllFormRelationLive()

    fun deleteForm(formEntity: FormRelation) {
        dataManager.deleteForm(formEntity)
            .doOnError(Timber::e)
            .subscribe()
            .autoDispose()
    }
}