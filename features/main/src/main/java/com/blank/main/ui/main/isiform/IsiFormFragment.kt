package com.blank.main.ui.main.isiform

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.blank.core.base.BaseFragment
import com.blank.core.utils.view.VerticalSpaceItemDecoration
import com.blank.main.BR
import com.blank.main.R
import com.blank.main.databinding.FragmentIsiFormBinding
import com.blank.models.local.entity.ResponseEntity
import com.blank.widget.utils.dpToPx
import com.blank.widget.utils.observe
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class IsiFormFragment : BaseFragment<FragmentIsiFormBinding, IsiFormViewModel>() {

    override fun layoutId(): Int = R.layout.fragment_isi_form
    override fun bindingVariable(): Int = BR.vmisiform
    override val viewModel: IsiFormViewModel by viewModels()

    @Inject
    lateinit var myAdapter: IsiFormAdapter
    private val args: IsiFormFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        myAdapter.setIsEdit(args.isEdit)

        binding?.rvIsiForm?.apply {
            layoutManager = LinearLayoutManager(requireContext())
            addItemDecoration(VerticalSpaceItemDecoration(requireContext() dpToPx 0))
            adapter = myAdapter
        }

        observe(viewModel.element) {
            val data = it
            if (data.size > 0) {
                data.add(data[data.size.minus(1)])
                myAdapter.setData(data)
            }
        }

        myAdapter.setSubmitClickListener { it, dataids ->
            if (args.isEdit) {
                val response =
                    ResponseEntity(args.form.formId, dataids.toMutableList(), it, args.responseId)
                viewModel.updateResponse(response)
                findNavController().popBackStack(R.id.isiFormFragment, true)
            } else {
                viewModel.addResponse(args.form.formId, it)
            }
        }

        observe(viewModel.loading) {
            if (!it) findNavController().popBackStack()
            binding?.pb?.isVisible = it
        }

        if (args.isEdit) {
            viewModel.getResponse(args.responseId)
        } else {
            viewModel.getAllElement(args.form.formId)
        }

        observe(viewModel.response) {
            val data = it.elements
            data.add(data[data.size.minus(1)])
            myAdapter.setData(data)
        }

        myAdapter.setDateClickListener { elementEntity, i ->
            val dialogDate = DatePickerFragment()
            dialogDate.setSaveDateListener { datePicker, year, mothn, day ->
                val element = elementEntity.copy()
                element.content = "$day/$mothn/$year"
                myAdapter.changeData(element, i)
            }
            dialogDate.show(childFragmentManager, "DatePicker")
        }
    }
}