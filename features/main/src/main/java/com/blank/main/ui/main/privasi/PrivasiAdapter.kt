package com.blank.main.ui.main.privasi

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.blank.core.base.BaseViewHolder
import com.blank.main.databinding.ItemFormsBinding
import com.blank.models.local.entity.FormRelation
import com.chauthai.swipereveallayout.ViewBinderHelper
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class PrivasiAdapter @Inject constructor() :
    RecyclerView.Adapter<PrivasiAdapter.PrivasiViewHolder>() {

    private val data: MutableList<FormRelation> = mutableListOf()
    private val viewBinderHelper = ViewBinderHelper()
    private lateinit var more: (FormRelation) -> Unit
    private lateinit var create: (FormRelation) -> Unit
    private lateinit var click: (FormRelation) -> Unit
    private lateinit var delete: (FormRelation) -> Unit

    fun setMoreActionClickListener(more: (FormRelation) -> Unit) {
        this.more = more
    }

    fun setDeleteActionClickListener(delete: (FormRelation) -> Unit) {
        this.delete = delete
    }

    fun setCreateActionClickListener(create: (FormRelation) -> Unit) {
        this.create = create
    }

    fun setClickListener(click: (FormRelation) -> Unit) {
        this.click = click
    }

    fun setData(data: List<FormRelation>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PrivasiViewHolder =
        PrivasiViewHolder(
            ItemFormsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

    override fun onBindViewHolder(holder: PrivasiViewHolder, position: Int) {
        val forms = data[position]
        viewBinderHelper.setOpenOnlyOne(true)
        viewBinderHelper.bind(holder.binding.swipelayout, forms.form.title)
        viewBinderHelper.closeLayout(forms.form.title)
        holder.binding.containerForms.setOnClickListener {
            click(forms)
        }
        holder.bind(forms, position)
    }

    override fun getItemCount(): Int = data.size

    inner class PrivasiViewHolder(val binding: ItemFormsBinding) :
        BaseViewHolder<FormRelation>(binding.root) {
        override fun bind(data: FormRelation, position: Int) {
            val dateString: String =
                SimpleDateFormat("dd MMM yyyy").format(Date(data.form.timestamp))

            binding.tvTitle.text = data.form.title
            binding.tvResponse.text = "${data.responses.size} Responses"
            binding.tvDate.text = dateString

            binding.btnMore.setOnClickListener {
                more(data)
            }

            binding.btnEdit.setOnClickListener {
                create(data)
            }
            binding.btnDelete.setOnClickListener {
                delete(data)
            }
        }
    }
}

