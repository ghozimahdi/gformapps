package com.blank.main.ui.main.privasi.createelement

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.blank.core.base.BaseFragment
import com.blank.main.BR
import com.blank.main.R
import com.blank.main.databinding.FragmentCreateElementBinding
import com.blank.models.local.entity.ElementEntity
import com.blank.widget.utils.observe
import com.flask.colorpicker.ColorPickerView
import com.flask.colorpicker.builder.ColorPickerDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class CreateElementFragment : BaseFragment<FragmentCreateElementBinding, CreateElementViewModel>() {

    override fun layoutId(): Int = R.layout.fragment_create_element
    override fun bindingVariable(): Int = BR.vmcreateelement
    override val viewModel: CreateElementViewModel by viewModels()
    private var colorInt = -15241473
    private val args: CreateElementFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.vColor?.setBackgroundColor(colorInt)

        val arrayAdapter = ArrayAdapter(
            requireContext(), android.R.layout.simple_dropdown_item_1line,
            arrayOf("String", "Integer", "Date")
        )
        binding?.spinnerDataType?.setAdapter(arrayAdapter)

        binding?.vColor?.setOnClickListener {
            ColorPickerDialogBuilder
                .with(context)
                .setTitle("Choose color")
                .initialColor(0xffffffff.toInt())
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setOnColorSelectedListener {
                    Timber.d(it.toString())
                }
                .setPositiveButton(
                    "ok"
                ) { dialog, selectedColor, allColors ->
                    colorInt = selectedColor
                    binding?.vColor?.setBackgroundColor(colorInt)
                }
                .setNegativeButton(
                    "cancel"
                ) { dialog, which ->
                    dialog.dismiss()
                }
                .build()
                .show()
        }

        observe(viewModel.status) {
            Toast.makeText(requireContext(), "$it", Toast.LENGTH_SHORT).show()
        }

        if (checkIfEdit()) {
            val element = args.element
            binding?.textFieldTitle?.editText?.setText(element?.title.toString())
            element?.color?.let { binding?.vColor?.setBackgroundColor(it) }
            binding?.textFieldSeq?.editText?.setText(element?.seqNo.toString())
            binding?.spinnerDataType?.setText(element?.dataType)

            findNavController().getBackStackEntry(R.id.createElementFragment)
                .savedStateHandle
                .set("edit", true)
        } else {
            findNavController().getBackStackEntry(R.id.createElementFragment)
                .savedStateHandle
                .set("edit", false)
        }
    }

    fun addElement() {
        val element = ElementEntity(
            colorInt,
            binding?.spinnerDataType?.text.toString(),
            binding?.textFieldSeq?.editText?.text.toString(),
            binding?.textFieldTitle?.editText?.text.toString(), "", "", args.form.formId, 0
        )
        viewModel.addElement(element)
        findNavController().popBackStack()
    }

    fun editElement() {
        val element = args.element?.copy(
            colorInt,
            binding?.spinnerDataType?.text.toString(),
            binding?.textFieldSeq?.editText?.text.toString(),
            binding?.textFieldTitle?.editText?.text.toString(), "", "", args.form.formId
        )
        element?.let { viewModel.editElement(it) }
        findNavController().popBackStack()
    }

    fun checkIfEdit() = args.element != null
}