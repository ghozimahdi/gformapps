package com.blank.main.ui.main.privasi.editforms

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.blank.core.base.BaseFragment
import com.blank.core.utils.view.VerticalSpaceItemDecoration
import com.blank.main.BR
import com.blank.main.R
import com.blank.main.databinding.FragmentEditFormBinding
import com.blank.widget.utils.dpToPx
import com.blank.widget.utils.observe
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class EditFormFragment : BaseFragment<FragmentEditFormBinding, EditFormViewModel>() {

    override fun layoutId(): Int = R.layout.fragment_edit_form
    override fun bindingVariable(): Int = BR.vmeditform
    override val viewModel: EditFormViewModel by viewModels()

    @Inject
    lateinit var myAdapter: EditFormAdapter
    private val args: EditFormFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observe(viewModel.formEntity) { form ->
            findNavController().currentBackStackEntry
                ?.savedStateHandle
                ?.set("form", form)

            binding?.tvTitle?.editText?.setText(form.title)

            observe(viewModel.fetchElementLive(form.formId), myAdapter::setData)

            myAdapter.setEditClickListener { element ->
                val action =
                    EditFormFragmentDirections.actionEditFormFragmentToCreateElementFragment(
                        form,
                        element
                    )
                findNavController().navigate(action)
            }

            myAdapter.setReviewClickListener {
                viewModel.removeElement(it)
            }
        }

        observe(viewModel.loading) {
            binding?.pbEditForm?.isVisible = it
        }
        viewModel.fetchFormByTitle(args.form.title)

        binding?.rvElements?.apply {
            layoutManager = LinearLayoutManager(requireContext())
            addItemDecoration(VerticalSpaceItemDecoration(requireContext() dpToPx 0))
            adapter = myAdapter
        }

        observe(viewModel.editTitleStatus) {
            if (it) findNavController().popBackStack()
        }
    }

    fun checkNotSame(): Boolean {
        val newTitle = binding?.tvTitle?.editText?.text.toString()
        val title = args.form.title
        if (title != newTitle)
            return true

        return false
    }

    fun editTitle() {
        val newTitle = binding?.tvTitle?.editText?.text.toString()
        val title = args.form.title

        if (checkNotSame())
            viewModel.editTitleForm(title, newTitle)
    }

    fun getTitle() = binding?.tvTitle?.editText?.text.toString()
}