package com.blank.main.ui.main.privasi.createelement

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.blank.core.base.BaseViewModel
import com.blank.core.data.DataManager
import com.blank.models.local.entity.ElementEntity
import timber.log.Timber

class CreateElementViewModel @ViewModelInject constructor(private val dataManager: DataManager) :
    BaseViewModel() {

    val status = MutableLiveData<Boolean>()

    fun addElement(elementEntity: ElementEntity) {
        dataManager.insertElement(elementEntity)
            .subscribe({
                status.value = it
            }, {
                status.value = false
            }).autoDispose()
    }

    fun editElement(elementEntity: ElementEntity) {
        dataManager.updateElement(elementEntity)
            .doOnError(Timber::e)
            .subscribe()
            .autoDispose()
    }
}