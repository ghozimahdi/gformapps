package com.blank.main.ui.main.isiform

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.blank.core.base.BaseViewModel
import com.blank.core.data.DataManager
import com.blank.models.local.entity.ElementEntity
import com.blank.models.local.entity.ResponseEntity
import io.reactivex.rxjava3.core.Observable
import timber.log.Timber

class IsiFormViewModel @ViewModelInject constructor(private val dm: DataManager) : BaseViewModel() {

    val element = MutableLiveData<MutableList<ElementEntity>>()
    val loading = MutableLiveData<Boolean>()
    val response = MutableLiveData<ResponseEntity>()

    fun getResponse(id: Int) {
        dm.getResponseById(id)
            .subscribe({
                response.value = it
            }, Timber::e)
            .autoDispose()
    }

    fun updateResponse(responseEntity: ResponseEntity) {
        dm.updateResponse(responseEntity)
            .doOnError(Timber::e)
            .subscribe()
            .autoDispose()
    }

    fun getAllElement(id: Int) {
        dm.getAllElementsByIdForm(id)
            .subscribe({
                element.value = it
            }, Timber::e)
            .autoDispose()
    }

    fun addResponse(formId: Int, dataElement: MutableList<ElementEntity>) {
        Observable.fromIterable(dataElement)
            .map {
                it.elementId
            }
            .toList()
            .toObservable()
            .flatMap {
                dm.insertResponse(ResponseEntity(formId, it, dataElement))
            }.doOnSubscribe {
                loading.value = true
            }
            .doOnError {
                loading.value = false
            }
            .subscribe({
                loading.value = false
            }, Timber::e)
            .autoDispose()
    }
}