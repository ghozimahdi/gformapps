package com.blank.main.ui.main

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.blank.core.base.BaseViewModel
import com.blank.core.data.DataManager
import com.blank.models.local.entity.FormEntity
import timber.log.Timber

class MainViewModel @ViewModelInject constructor(private val dataManager: DataManager) :
    BaseViewModel() {

    val titleNotExists = MutableLiveData<Boolean>()

    fun addForm(formEntity: FormEntity) {
        dataManager.insertForm(formEntity)
            .doOnError(Timber::e)
            .subscribe()
            .autoDispose()
    }

    fun checkTitleifExists(title: String) {
        dataManager.getFormByTitle(title)
            .subscribe({
                titleNotExists.value = false
            }, {
                titleNotExists.value = true
            })
            .autoDispose()
    }
}