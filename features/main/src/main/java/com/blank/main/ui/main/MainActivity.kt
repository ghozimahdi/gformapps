package com.blank.main.ui.main

import android.app.Dialog
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.blank.core.base.BaseActivity
import com.blank.core.navigation.Navigator
import com.blank.main.R
import com.blank.main.databinding.ActivityMainBinding
import com.blank.main.ui.main.privasi.createelement.CreateElementFragment
import com.blank.main.ui.main.privasi.editforms.EditFormFragment
import com.blank.main.ui.main.privasi.editforms.EditFormFragmentDirections
import com.blank.models.local.entity.FormEntity
import com.blank.widget.utils.hide
import com.blank.widget.utils.observe
import com.blank.widget.utils.show
import com.google.android.material.textfield.TextInputLayout
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    private lateinit var navController: NavController

    @Inject
    lateinit var navigator: Navigator
    private var addItem: MenuItem? = null
    private var addElement: MenuItem? = null
    private var saveItem: MenuItem? = null
    private var createItem: MenuItem? = null
    override fun layoutId(): Int = R.layout.activity_main
    override val viewModel: MainViewModel by viewModels()
    override fun bindingVariable(): Int = 0
    private var dialog: Dialog? = null
    private var txt: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navController = findNavController(R.id.navHost)
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)

//        binding?.bottomNavView?.setupWithNavController(navController)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.privateFragment -> {
                    title = getString(R.string.forms)
                    createItem?.hide()
                    addItem?.show()
                    saveItem?.hide()
                    addElement?.hide()
                }
                R.id.devFragment -> {
                    createItem?.hide()
                    addItem?.hide()
                    saveItem?.hide()
                    addElement?.hide()
                }
                R.id.editFormFragment -> {
                    createItem?.hide()
                    addItem?.show()
                    saveItem?.show()
                    addElement?.hide()
                }
                R.id.createElementFragment -> {
                    navController.getBackStackEntry(R.id.createElementFragment)
                        .savedStateHandle
                        .getLiveData<Boolean>("edit")
                        .observe(this) {
                            if (it) {
                                createItem?.hide()
                                addItem?.hide()
                                saveItem?.show()
                                addElement?.hide()
                            } else {
                                createItem?.hide()
                                addItem?.hide()
                                saveItem?.hide()
                                addElement?.show()
                            }
                        }
                }
                R.id.isiFormFragment -> {
                    createItem?.hide()
                    addItem?.hide()
                    saveItem?.hide()
                    addElement?.hide()
                }
                R.id.responseFormsFragment -> {
                    createItem?.hide()
                    addItem?.hide()
                    saveItem?.hide()
                    addElement?.hide()
                }
            }
        }

        observe(viewModel.titleNotExists) {
            if (it) {
                if (navController.currentDestination?.id == R.id.editFormFragment) {
                    val navHostFragment = supportFragmentManager.findFragmentById(R.id.navHost)
                    val editFormFragment =
                        navHostFragment?.childFragmentManager?.fragments?.last() as EditFormFragment

                    editFormFragment.editTitle()
                } else {
                    txt?.let { it1 ->
                        val form = FormEntity(
                            Date().time,
                            0,
                            0,
                            it1
                        )
                        viewModel.addForm(form)
                    }
                    dialog?.dismiss()
                }
            } else {
                Toast.makeText(this, "Form sudah ada", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        addItem = menu.findItem(R.id.iAdd)
        createItem = menu.findItem(R.id.iCreate)
        createItem?.hide()
        saveItem = menu.findItem(R.id.iSave)
        saveItem?.hide()
        addElement = menu.findItem(R.id.iAddElement)
        addElement?.hide()
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.iAdd -> {
                if (navController.currentDestination?.id == R.id.editFormFragment) {
                    val form = navController.getBackStackEntry(R.id.editFormFragment)
                        .savedStateHandle
                        .get<FormEntity>("form")
                    val action = form?.let {
                        EditFormFragmentDirections.actionEditFormFragmentToCreateElementFragment(
                            it
                        )
                    }
                    action?.let { navController.navigate(it) }
                } else {
                    val v = layoutInflater.inflate(R.layout.dialog_create_form, null)
                    val txtF = v.findViewById<TextInputLayout>(R.id.textField)

                    val builder = AlertDialog.Builder(this)
                    builder.setView(v)
                        .setPositiveButton(
                            R.string.add
                        ) { dialog, _ ->
                            txt = txtF.editText?.text.toString()
                            txt?.let {
                                viewModel.checkTitleifExists(it)
                            }
                        }
                        .setNegativeButton(
                            R.string.cancel
                        ) { dialog, _ ->
                            dialog.cancel()
                        }
                    dialog = builder.create()
                    dialog?.show()
                }
            }

            R.id.iSave -> {
                if (navController.currentDestination?.id == R.id.createElementFragment) {
                    val navHostFragment = supportFragmentManager.findFragmentById(R.id.navHost)
                    val createFragment =
                        navHostFragment?.childFragmentManager?.fragments?.last() as CreateElementFragment
                    createFragment.editElement()
                } else if (navController.currentDestination?.id == R.id.editFormFragment) {
                    editFormTitle()
                }
            }

            R.id.iAddElement -> {
                if (navController.currentDestination?.id == R.id.createElementFragment) {
                    val navHostFragment = supportFragmentManager.findFragmentById(R.id.navHost)
                    val createFragment =
                        navHostFragment?.childFragmentManager?.fragments?.last() as CreateElementFragment
                    createFragment.addElement()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun editFormTitle() {
        if (navController.currentDestination?.id == R.id.editFormFragment) {
            val navHostFragment = supportFragmentManager.findFragmentById(R.id.navHost)
            val editFormFragment =
                navHostFragment?.childFragmentManager?.fragments?.last() as EditFormFragment

            if (editFormFragment.checkNotSame())
                viewModel.checkTitleifExists(editFormFragment.getTitle())
            else
                Toast.makeText(this, "Form sudah ada", Toast.LENGTH_SHORT)
                    .show()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp()
    }
}