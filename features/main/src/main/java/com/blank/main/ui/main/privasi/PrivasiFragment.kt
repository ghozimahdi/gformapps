package com.blank.main.ui.main.privasi

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.blank.core.base.BaseFragment
import com.blank.main.BR
import com.blank.main.R
import com.blank.main.databinding.FragmentPrivasiBinding
import com.blank.widget.utils.observe
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PrivasiFragment : BaseFragment<FragmentPrivasiBinding, PrivasiViewModel>() {

    override fun layoutId(): Int = R.layout.fragment_privasi
    override fun bindingVariable(): Int = BR.vmprivasi
    override val viewModel: PrivasiViewModel by viewModels()

    @Inject
    lateinit var myAdapter: PrivasiAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.rvForms?.apply {
            layoutManager = LinearLayoutManager(requireContext())
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = myAdapter
        }

        myAdapter.setMoreActionClickListener {
            val action = PrivasiFragmentDirections.actionPrivateFragmentToEditFormFragment(it.form)
            findNavController().navigate(action)
        }

        myAdapter.setCreateActionClickListener {
            val action = PrivasiFragmentDirections
                .actionPrivateFragmentToIsiFormFragment(it.form)
            findNavController().navigate(action)
        }

        myAdapter.setClickListener {
            val action =
                PrivasiFragmentDirections.actionPrivateFragmentToResponseFormsFragment(it.form)
            findNavController().navigate(action)
        }

        myAdapter.setDeleteActionClickListener { form ->
            activity?.let {
                val builder = AlertDialog.Builder(it)
                    .setMessage(getString(R.string.msg_delete))
                    .setPositiveButton(getString(R.string.iya)) { dialog, i ->
                        viewModel.deleteForm(form)
                        dialog.dismiss()
                    }.setNegativeButton(getString(R.string.tidak)) { dialog, i ->
                        dialog.cancel()
                    }
                val alert = builder.create()
                alert.show()
            }
        }

        observe(viewModel.getFormsRelationLive()) {
            myAdapter.setData(it)
        }
    }
}