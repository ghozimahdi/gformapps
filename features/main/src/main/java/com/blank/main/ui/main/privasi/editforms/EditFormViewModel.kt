package com.blank.main.ui.main.privasi.editforms

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.blank.core.base.BaseViewModel
import com.blank.core.data.DataManager
import com.blank.models.local.entity.ElementEntity
import com.blank.models.local.entity.FormEntity
import timber.log.Timber

class EditFormViewModel @ViewModelInject constructor(private val dataManager: DataManager) :
    BaseViewModel() {

    val loading = MutableLiveData(true)
    val element = MutableLiveData<MutableList<ElementEntity>>()
    val formEntity = MutableLiveData<FormEntity>()
    val editTitleStatus = MutableLiveData<Boolean>()

    fun fetchElement(id: Int) {
        dataManager.getAllElementsByIdForm(id)
            .subscribe(element::setValue, Timber::e)
            .autoDispose()
    }

    fun fetchElementLive(id: Int) = dataManager.getAllElementsLive(id)

    fun fetchFormByTitle(title: String) {
        dataManager.getFormByTitle(title)
            .doOnSubscribe {
                loading.value = true
            }
            .subscribe({
                formEntity.value = it
                loading.value = false
            }, {
                Timber.e(it)
                loading.value = false
            })
            .autoDispose()
    }

    fun removeElement(element: ElementEntity) {
        dataManager.delete(element)
            .doOnError(Timber::e)
            .subscribe()
            .autoDispose()
    }

    fun editTitleForm(title: String, newTitle: String) {
        dataManager.updateTitleForm(title, newTitle)
            .doOnError(Timber::e)
            .subscribe({
                editTitleStatus.value = it
            }, {
                editTitleStatus.value = false
            })
            .autoDispose()
    }
}