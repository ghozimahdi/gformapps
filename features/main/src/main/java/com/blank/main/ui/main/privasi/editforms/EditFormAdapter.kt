package com.blank.main.ui.main.privasi.editforms

import android.text.InputType
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.blank.main.databinding.ItemEditFormBinding
import com.blank.models.local.DataType
import com.blank.models.local.entity.ElementEntity
import javax.inject.Inject

class EditFormAdapter @Inject constructor() :
    RecyclerView.Adapter<EditFormAdapter.EditFormViewHolder>() {
    private val data = mutableListOf<ElementEntity>()
    private lateinit var editListener: (ElementEntity) -> Unit
    private lateinit var removeListener: (ElementEntity) -> Unit

    fun setData(data: List<ElementEntity>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun setEditClickListener(editListener: (ElementEntity) -> Unit) {
        this.editListener = editListener
    }

    fun setReviewClickListener(removeListener: (ElementEntity) -> Unit) {
        this.removeListener = removeListener
    }

    fun removeElement(elementEntity: ElementEntity, position: Int) {
        this.data.remove(elementEntity)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, data.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EditFormViewHolder =
        EditFormViewHolder(
            ItemEditFormBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: EditFormViewHolder, position: Int) {
        val item = data[position]
        holder.bind(item, position)
    }

    override fun getItemCount(): Int = data.size

    inner class EditFormViewHolder(private val v: ItemEditFormBinding) :
        RecyclerView.ViewHolder(v.root) {
        fun bind(data: ElementEntity, position: Int) {
            v.bg.setBackgroundColor(data.color)
            v.tvTitle.text = data.title
            v.etContent.hint = "Masukan ${data.title}"

            when (data.dataType) {
                DataType.TEXT.type -> {
                    v.etContent.inputType = InputType.TYPE_CLASS_TEXT
                }
                DataType.NUM.type -> {
                    v.etContent.inputType = InputType.TYPE_CLASS_NUMBER
                }
                DataType.DATE.type -> {

                }
                DataType.PICTURE.type -> {

                }
            }

            v.ibDelete.setOnClickListener {
                removeListener(data)
            }

            v.ibEdit.setOnClickListener {
                editListener(data)
            }
        }

    }
}