package com.blank.main.ui.main.isiform

import android.text.InputType
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.RecyclerView
import com.blank.main.databinding.ItemButtonSubmitBinding
import com.blank.main.databinding.ItemIsiFormBinding
import com.blank.models.local.DataType
import com.blank.models.local.entity.ElementEntity
import javax.inject.Inject

class IsiFormAdapter @Inject constructor() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val data = mutableListOf<ElementEntity>()
    private val dataIds = hashSetOf<Int>()
    private val NORMAL_TYPE = 100
    private val SUBMI_TYPE = 101
    private var isEdit = false
    private lateinit var submit: (MutableList<ElementEntity>, HashSet<Int>) -> Unit
    private lateinit var date: (ElementEntity, Int) -> Unit

    fun setIsEdit(isEdit: Boolean) {
        this.isEdit = isEdit
    }

    fun setSubmitClickListener(submit: (MutableList<ElementEntity>, HashSet<Int>) -> Unit) {
        this.submit = submit
    }

    fun setDateClickListener(date: (ElementEntity, Int) -> Unit) {
        this.date = date
    }

    fun setData(data: MutableList<ElementEntity>) {
        this.data.clear()
        this.data.addAll(data)
        this.dataIds.clear()
        notifyDataSetChanged()
    }

    fun remove(data: ElementEntity, position: Int) {
        this.data.remove(data)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, this.data.size)
    }

    fun changeData(elementEntity: ElementEntity, position: Int) {
        this.data[position] = elementEntity
        notifyItemChanged(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            NORMAL_TYPE -> {
                return IsiFormViewHolder(
                    ItemIsiFormBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
            SUBMI_TYPE -> {
                return SubmitViewHolder(
                    ItemButtonSubmitBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
            else -> return IsiFormViewHolder(
                ItemIsiFormBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = data[position]
        if (holder is IsiFormViewHolder)
            holder.bind(item, position)
        else if (holder is SubmitViewHolder)
            holder.bind(item, position)

        this.dataIds.add(item.elementId)
    }

    override fun getItemCount(): Int = data.size

    override fun getItemViewType(position: Int): Int =
        if (position > 0 && data[position].title == data[position.minus(1)].title) SUBMI_TYPE else NORMAL_TYPE

    inner class SubmitViewHolder(private val v: ItemButtonSubmitBinding) :
        RecyclerView.ViewHolder(v.root) {
        fun bind(data: ElementEntity, position: Int) {
            v.btnSubmit.setOnClickListener {
                remove(this@IsiFormAdapter.data[this@IsiFormAdapter.data.size.minus(1)], position)
                submit(this@IsiFormAdapter.data, this@IsiFormAdapter.dataIds)
            }

            if (isEdit) {
                v.btnSubmit.text = "Save"
            } else {
                v.btnSubmit.text = "Submit"
            }
        }
    }

    inner class IsiFormViewHolder(private val v: ItemIsiFormBinding) :
        RecyclerView.ViewHolder(v.root) {
        fun bind(data: ElementEntity, position: Int) {
            val element = data
            v.bg.setBackgroundColor(data.color)
            v.tvTitle.text = data.title
            v.etContent.hint =
                "Masukan ${data.title}"
            v.etContent.setText(data.content)
            v.etContent.addTextChangedListener {
                element.content = it.toString()
                this@IsiFormAdapter.data[position] = element
            }

            when (data.dataType) {
                DataType.TEXT.type -> {
                    v.etContent.inputType = InputType.TYPE_CLASS_TEXT
                }
                DataType.NUM.type -> {
                    v.etContent.inputType = InputType.TYPE_CLASS_NUMBER
                }
                DataType.DATE.type -> {
                    v.etContent.isFocusable = false
                    v.etContent.setOnClickListener {
                        date(data, position)
                    }
                }
                DataType.PICTURE.type -> {

                }
            }
        }
    }
}