package com.blank.main.ui.main.response

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.blank.core.base.BaseFragment
import com.blank.core.utils.view.VerticalSpaceItemDecoration
import com.blank.main.BR
import com.blank.main.R
import com.blank.main.databinding.FragmentResponseFormsBinding
import com.blank.main.ui.main.isiform.ResponseFormAdapter
import com.blank.models.local.entity.ResponseEntity
import com.blank.widget.utils.dpToPx
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class ResponseFormsFragment :
    BaseFragment<FragmentResponseFormsBinding, ResponseFromsViewModel>(),
    ResponseFormsNavigator {

    override fun layoutId(): Int = R.layout.fragment_response_forms
    override fun bindingVariable(): Int = BR.vmresponseform
    override val viewModel: ResponseFromsViewModel by viewModels()
    private val args: ResponseFormsFragmentArgs by navArgs()

    @Inject
    lateinit var myAdapter: ResponseFormAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setNavigator(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.rvResponse?.apply {
            layoutManager = LinearLayoutManager(requireContext())
            addItemDecoration(VerticalSpaceItemDecoration(requireContext() dpToPx 0))
            adapter = myAdapter
        }

        myAdapter.setEditListener {
            val action =
                ResponseFormsFragmentDirections.actionResponseFormsFragmentToIsiFormFragment(
                    args.form,
                    true,
                    it.responsesId
                )
            findNavController().navigate(action)
        }

        myAdapter.setDeleteListener { response, posisi ->
            activity?.let {
                val builder = AlertDialog.Builder(it)
                    .setMessage(getString(R.string.msg_response_delete))
                    .setPositiveButton(getString(R.string.iya)) { dialog, i ->
                        viewModel.deleteResponse(response)
                        myAdapter.deleteData(response, posisi)
                        dialog.dismiss()
                    }.setNegativeButton(getString(R.string.tidak)) { dialog, i ->
                        dialog.cancel()
                    }
                val alert = builder.create()
                alert.show()
            }
        }

        viewModel.getResponse(args.form.formId)
        binding?.tv1?.text = args.form.title
    }

    override fun onDestroyView() {
        binding?.rvResponse?.adapter = null
        super.onDestroyView()
    }

    override fun resultResponseForm(responses: MutableList<ResponseEntity>) {
        Timber.e(responses.toString())
        myAdapter.setData(responses)
    }
}