package com.blank.main.ui.main.isiform

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.blank.core.base.BaseViewHolder
import com.blank.core.utils.AutoUpdatableAdapter
import com.blank.main.databinding.ItemIsiFormBinding
import com.blank.main.databinding.ItemResponseBinding
import com.blank.models.local.entity.ResponseEntity
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import kotlin.properties.Delegates

class ResponseFormAdapter @Inject constructor(@ApplicationContext private val context: Context) :
    RecyclerView.Adapter<ResponseFormAdapter.ResponseFormViewHolder>(), AutoUpdatableAdapter {

    private val data: MutableList<ResponseEntity> by Delegates.observable(mutableListOf()) { prop, oldList, newList ->
        autoNotify(oldList, newList) { o, n -> o.elementsId == n.elementsId }
    }
    private lateinit var edit: (ResponseEntity) -> Unit
    private lateinit var delete: (ResponseEntity, Int) -> Unit

    fun setEditListener(edit: (ResponseEntity) -> Unit) {
        this.edit = edit
    }

    fun setDeleteListener(delete: (ResponseEntity, Int) -> Unit) {
        this.delete = delete
    }

    fun setData(data: MutableList<ResponseEntity>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun deleteData(responseEntity: ResponseEntity, position: Int) {
        this.data.remove(responseEntity)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, data.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResponseFormViewHolder {
        return ResponseFormViewHolder(
            ItemResponseBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ResponseFormViewHolder, position: Int) {
        val item = data[position]
        holder.bind(item, position)
    }

    override fun getItemCount(): Int = data.size


    inner class ResponseFormViewHolder(private val binding: ItemResponseBinding) :
        BaseViewHolder<ResponseEntity>(binding.root) {
        override fun bind(data: ResponseEntity, position: Int) {
            binding.tvResponseTitle.text = "Response ${position.plus(1)}"
            binding.btnEdit.setOnClickListener {
                edit(data)
            }
            binding.btnDelete.setOnClickListener {
                delete(data, position)
            }
            binding.containerElement.removeAllViews()
            data.elements.forEach {
                val v = ItemIsiFormBinding.inflate(LayoutInflater.from(context), null, false)
                v.bg.setBackgroundColor(it.color)
                v.tvTitle.text = it.title
                v.etContent.setText(it.content)
                v.etContent.isEnabled = false

                binding.containerElement.addView(v.root)
            }
        }
    }
}