package com.blank.main.ui.main.response

import androidx.hilt.lifecycle.ViewModelInject
import com.blank.core.base.BaseViewModel
import com.blank.core.data.DataManager
import com.blank.models.local.entity.ResponseEntity
import timber.log.Timber

class ResponseFromsViewModel @ViewModelInject constructor(private val dm: DataManager) :
    BaseViewModel() {

    private lateinit var navigator: ResponseFormsNavigator

    fun setNavigator(navigator: ResponseFormsNavigator) {
        this.navigator = navigator
    }

    fun getResponse(formId: Int) {
        dm.getResponseEntity(formId)
            .subscribe({
                navigator.resultResponseForm(it)
            }, Timber::e)
            .autoDispose()
    }

    fun deleteResponse(responseEntity: ResponseEntity) {
        dm.deleteResponse(responseEntity)
            .subscribe()
            .autoDispose()
    }
}