package com.blank.core.di.module

import android.content.Context
import androidx.room.Room
import com.blank.core.data.local.db.AppDatabase
import com.blank.core.data.local.db.helper.*
import com.blank.core.utils.DB_NAME
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import net.sqlcipher.database.SQLiteDatabase
import net.sqlcipher.database.SupportFactory

/**
 * Created by GhoziMahdi on 09/12/2019.
 * Email : -
 * No Tpln : -
 * Profesi : Mobile Engineer Development
 */

@Module
@InstallIn(SingletonComponent::class)
object DbModule {

    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase {
        val passphrase: ByteArray = SQLiteDatabase.getBytes(DB_NAME.toCharArray())
        val factory = SupportFactory(passphrase)

        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            DB_NAME
        ).fallbackToDestructiveMigration()
            .allowMainThreadQueries()
//            .openHelperFactory(factory)
            .build()
    }

    @Provides
    fun provideResponseHelper(responseDbHelper: ResponseDbHelper): ResponseHelper = responseDbHelper

    @Provides
    fun provideFormHelper(formDbHelper: FormDbHelper): FormHelper = formDbHelper

    @Provides
    fun provideElementHelper(elementDbHelper: ElementDbHelper): ElementHelper = elementDbHelper
}