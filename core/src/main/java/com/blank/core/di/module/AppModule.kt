package com.blank.core.di.module

import com.blank.core.data.AppDataManager
import com.blank.core.data.DataManager
import com.blank.core.navigation.Navigator
import com.blank.core.navigation.NavigatorProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Provides
    fun provideNavigator(navigatorProvider: NavigatorProvider): Navigator = navigatorProvider

    @Provides
    fun provideAppManager(appDataManager: AppDataManager): DataManager = appDataManager
}