package com.blank.core.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder<T>(v: View) : RecyclerView.ViewHolder(v) {
    abstract fun bind(data: T, position: Int)
}