package com.blank.core.navigation

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NavigatorProvider @Inject constructor(@ApplicationContext private val context: Context) :
    Navigator {
}