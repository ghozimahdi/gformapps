package com.blank.core.utils

const val BASE_URL = "https://api.themoviedb.org/"
const val HOSTNAME = "api.themoviedb.org"
const val API_KEY = "52b26ca302bf3a2dd288dd286f6c633d"
const val ERROR_NOCONNECTION = "No internet connection"
const val NO_ITEM_PAGING = "No Data Found"
const val NETWORK_PAGE_SIZE = 20
const val KEY_REQ_MOVIES = "reqMovies"
const val KEY_ITEM_MOVIES = "item"

const val API_STATUS_CODE_LOCAL_ERROR = 0
const val DB_NAME = "blank.db"
const val NULL_INDEX = -1L
const val PREF_NAME = "blank_pref"
const val SEED_DATABASE_OPTIONS = "seed/options.json"
const val SEED_DATABASE_QUESTIONS = "seed/questions.json"
const val STATUS_CODE_FAILED = "failed"
const val STATUS_CODE_SUCCESS = "success"
const val TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss"
const val TAG = "FrameworkBlank"