package com.blank.core.utils

import kotlin.reflect.KClass
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.jvm.javaField

@Target(AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class Map(val to: String)

data class Medium(
    @Map("url")
    val url: String,
    @Map("img")
    val img: String,
    @Map("imgCls")
    val imgCls: ImageIn
)

data class MediumModel(
    val url: String? = null,
    val img: String? = null,
    val imgCls: ImageOut? = null
)

data class ImageIn(@Map("url") val url: String? = null)

data class ImageOut(@Map("url") val url: String? = null)

fun generate(
    data: Medium,
    source: KClass<out Medium>,
    target: KClass<MediumModel>
): MediumModel {
    return MediumModel().apply {
        source.declaredMemberProperties.forEach {
            if (it.findAnnotation<Map>() != null) {
                val value = it.findAnnotation<Map>()

                if (value != null) {
                    if (value.to.isNotEmpty()) {
                        try {
                            val sourceProperty = it.javaField
                            sourceProperty?.isAccessible = true

                            val te = sourceProperty?.get(data)
                            if (te?.javaClass?.isMemberClass == true) {

                            } else {
                                val targetProperty = target.java.getDeclaredField(value.to)
                                targetProperty.isAccessible = true
                                targetProperty.set(this, sourceProperty?.get(data))
                            }
                        } catch (e: NoSuchFieldError) {
                            e.printStackTrace()
                        } catch (e: IllegalArgumentException) {
                            e.printStackTrace()
                        }
                    }
                }
            }
        }
    }
}