package com.blank.core.utils

import androidx.room.TypeConverter
import com.blank.models.local.entity.ElementEntity
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import java.lang.reflect.Type

object Converters {
    @TypeConverter
    @JvmStatic
    fun fromString(value: String): List<Int> {
        val listType: Type = object : TypeToken<List<Int>>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    @JvmStatic
    fun fromArrayList(list: List<Int>): String = Gson().toJson(list)

    @TypeConverter
    @JvmStatic
    fun fromStringElement(value: String): List<ElementEntity> {
        val listType: Type = object : TypeToken<List<ElementEntity>>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    @JvmStatic
    fun fromListElement(list: List<ElementEntity>): String = Gson().toJson(list)
}