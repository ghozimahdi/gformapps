package com.blank.core.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.blank.core.data.local.db.dao.ElementDao
import com.blank.core.data.local.db.dao.FormDao
import com.blank.core.data.local.db.dao.ResponseDao
import com.blank.core.utils.Converters
import com.blank.models.local.entity.ElementEntity
import com.blank.models.local.entity.FormEntity
import com.blank.models.local.entity.ResponseEntity

/**
 * Created by GhoziMahdi on 09/12/2019.
 * Email : -
 * No Tpln : -
 * Profesi : Mobile Engineer Development
 */

@Database(
    entities = [FormEntity::class, ResponseEntity::class, ElementEntity::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun formDao(): FormDao
    abstract fun elementDao(): ElementDao
    abstract fun responseDao(): ResponseDao
}