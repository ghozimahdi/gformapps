package com.blank.core.data.local.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.blank.core.base.BaseDao
import com.blank.models.local.entity.ElementEntity

@Dao
interface ElementDao : BaseDao<ElementEntity> {
    @Query("Select * From Elements where formEntityId = :id")
    fun getAllElementsByIdForm(id: Int): MutableList<ElementEntity>

    @Query("Select * From Elements where formEntityId = :id")
    fun getAllElementsLive(id: Int): LiveData<MutableList<ElementEntity>>

    @Query("Select * From Elements where elementId in (:ids)")
    fun getElementByList(ids: List<Int>): MutableList<ElementEntity>

    @Query("Delete From Elements where elementId in (:ids)")
    fun deleteElementByList(ids: List<Int>)
}