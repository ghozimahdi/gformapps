package com.blank.core.data.local.db.helper

import androidx.lifecycle.LiveData
import com.blank.core.data.local.db.AppDatabase
import com.blank.core.utils.rx.observableIo
import com.blank.models.local.entity.ElementEntity
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ElementDbHelper @Inject constructor(private val appDb: AppDatabase) : ElementHelper {
    override fun insertElement(elementEntity: ElementEntity): Observable<Boolean> =
        Observable.fromCallable {
            appDb.elementDao().insert(elementEntity)
            true
        }.observableIo()

    override fun getAllElementsByIdForm(id: Int): Observable<MutableList<ElementEntity>> =
        Observable.fromCallable {
            appDb.elementDao().getAllElementsByIdForm(id)
        }.observableIo()

    override fun getElementByList(ids: List<Int>): Observable<MutableList<ElementEntity>> =
        Observable.fromCallable {
            appDb.elementDao().getElementByList(ids)
        }.observableIo()

    override fun delete(elementEntity: ElementEntity): Observable<Boolean> =
        Observable.fromCallable {
            appDb.elementDao().delete(elementEntity)
            true
        }.observableIo()

    override fun getAllElementsLive(id: Int): LiveData<MutableList<ElementEntity>> =
        appDb.elementDao().getAllElementsLive(id)

    override fun updateElement(elementEntity: ElementEntity): Observable<Boolean> =
        Observable.fromCallable {
            appDb.elementDao().update(elementEntity)
            true
        }.observableIo()

    override fun deleteElementByList(ids: List<Int>): Observable<Boolean> =
        Observable.fromCallable {
            appDb.elementDao().deleteElementByList(ids)
            true
        }.observableIo()
}