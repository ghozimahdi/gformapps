package com.blank.core.data.local.db.helper

import com.blank.models.local.entity.ResponseEntity
import io.reactivex.rxjava3.core.Observable

interface ResponseHelper {
    fun insertResponse(responseEntity: ResponseEntity): Observable<Boolean>
    fun updateResponse(responseEntity: ResponseEntity): Observable<Boolean>
    fun getResponseEntity(formId: Int): Observable<MutableList<ResponseEntity>>
    fun getResponseByDesc(formId: Int): Observable<ResponseEntity>
    fun getResponseById(id: Int): Observable<ResponseEntity>
    fun deleteResponse(responseEntity: ResponseEntity): Observable<Boolean>
    fun deleteResponseByList(ids: List<Int>): Observable<Boolean>
}