package com.blank.core.data.local.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.blank.core.base.BaseDao
import com.blank.models.local.entity.FormEntity
import com.blank.models.local.entity.FormRelation

/**
 * Created by GhoziMahdi on 09/12/2019.
 * Email : -
 * No Tpln : -
 * Profesi : Mobile Engineer Development
 */

@Dao
interface FormDao : BaseDao<FormEntity> {
    @Transaction
    @Query("Select * From Forms")
    fun getFormRelation(): MutableList<FormRelation>

    @Transaction
    @Query("Select * From Forms")
    fun getFormRelationLive(): LiveData<MutableList<FormRelation>>

    @Query("Select * From Forms")
    fun getFormLive(): LiveData<MutableList<FormEntity>>

    @Query("Select * From Forms where title = :title limit 1")
    fun getFormByTitle(title: String): FormEntity

    @Query("update forms set title = :newTitle where title = :title")
    fun updateTitleForm(title: String, newTitle: String)
}