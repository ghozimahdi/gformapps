package com.blank.core.data.local.db.helper

import androidx.lifecycle.LiveData
import com.blank.core.data.local.db.AppDatabase
import com.blank.core.utils.rx.observableIo
import com.blank.models.local.entity.FormEntity
import com.blank.models.local.entity.FormRelation
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FormDbHelper @Inject constructor(private val appDb: AppDatabase) : FormHelper {
    override fun insertForm(form: FormEntity): Observable<Boolean> =
        Observable.fromCallable {
            appDb.formDao().insert(form)
            true
        }.observableIo()

    override fun deleteForm(form: FormEntity): Observable<Boolean> =
        Observable.fromCallable {
            appDb.formDao().delete(form)
            true
        }

    override fun getAllForm(): Observable<MutableList<FormRelation>> =
        Observable.fromCallable {
            appDb.formDao().getFormRelation()
        }.observableIo()

    override fun getAllFormLive(): LiveData<MutableList<FormEntity>> =
        appDb.formDao().getFormLive()

    override fun getAllFormRelationLive(): LiveData<MutableList<FormRelation>> =
        appDb.formDao().getFormRelationLive()

    override fun getFormByTitle(title: String): Observable<FormEntity> =
        Observable.fromCallable {
            appDb.formDao().getFormByTitle(title)
        }.observableIo()


    override fun updateTitleForm(title: String, newTitle: String): Observable<Boolean> =
        Observable.fromCallable {
            appDb.formDao().updateTitleForm(title, newTitle)
            true
        }.observableIo()
}