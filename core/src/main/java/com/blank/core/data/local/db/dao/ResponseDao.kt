package com.blank.core.data.local.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.blank.core.base.BaseDao
import com.blank.models.local.entity.ResponseEntity

@Dao
interface ResponseDao : BaseDao<ResponseEntity> {
    @Query("Select * From Responses where formEntityId = :formId order by responsesId desc limit 1")
    fun getResponseByDesc(formId: Int): ResponseEntity

    @Query("Select * From Responses where responsesId = :id limit 1")
    fun getResponseById(id: Int): ResponseEntity

    @Query("Select * From Responses where formEntityId = :formId")
    fun getResponseEntity(formId: Int): MutableList<ResponseEntity>

    @Query("Delete From Responses where responsesId in (:ids)")
    fun deleteResponseByList(ids: List<Int>)
}