package com.blank.core.data.local.db

import androidx.lifecycle.LiveData
import com.blank.core.data.local.db.helper.ElementHelper
import com.blank.core.data.local.db.helper.FormHelper
import com.blank.core.data.local.db.helper.ResponseHelper
import com.blank.models.local.entity.ElementEntity
import com.blank.models.local.entity.FormEntity
import com.blank.models.local.entity.FormRelation
import com.blank.models.local.entity.ResponseEntity
import io.reactivex.rxjava3.core.Observable
import timber.log.Timber

open class AppDbHelper(
    private val formHelper: FormHelper,
    private val elementHelper: ElementHelper,
    private val responseHelper: ResponseHelper
) : DbHelper {
    override fun insertForm(form: FormEntity): Observable<Boolean> = formHelper.insertForm(form)
    override fun insertResponse(responseEntity: ResponseEntity): Observable<Boolean> =
        responseHelper.insertResponse(responseEntity)

    override fun updateResponse(responseEntity: ResponseEntity): Observable<Boolean> =
        responseHelper.updateResponse(responseEntity)

    override fun insertElement(elementEntity: ElementEntity): Observable<Boolean> =
        elementHelper.insertElement(elementEntity)

    override fun deleteForm(form: FormRelation): Observable<Boolean> =
        deleteForm(form.form)
            .flatMapIterable { form.responses }
            .flatMap {
                deleteElementByList(it.elementsId)
            }
            .flatMapIterable { form.responses }
            .map {
                it.responsesId
            }
            .toList()
            .toObservable()
            .flatMap {
                deleteResponseByList(it)
            }

    override fun deleteForm(form: FormEntity): Observable<Boolean> = formHelper.deleteForm(form)

    override fun getAllForm(): Observable<MutableList<FormRelation>> = formHelper.getAllForm()
    override fun getAllFormLive(): LiveData<MutableList<FormEntity>> = formHelper.getAllFormLive()
    override fun getAllFormRelationLive(): LiveData<MutableList<FormRelation>> =
        formHelper.getAllFormRelationLive()

    override fun getFormByTitle(title: String): Observable<FormEntity> =
        formHelper.getFormByTitle(title)

    override fun getAllElementsByIdForm(id: Int): Observable<MutableList<ElementEntity>> =
        elementHelper.getAllElementsByIdForm(id)

    override fun getElementByList(ids: List<Int>): Observable<MutableList<ElementEntity>> =
        elementHelper.getElementByList(ids)

    override fun delete(elementEntity: ElementEntity): Observable<Boolean> =
        elementHelper.delete(elementEntity)

    override fun updateTitleForm(title: String, newTitle: String): Observable<Boolean> =
        formHelper.updateTitleForm(title, newTitle)

    override fun getAllElementsLive(id: Int): LiveData<MutableList<ElementEntity>> =
        elementHelper.getAllElementsLive(id)

    override fun updateElement(elementEntity: ElementEntity): Observable<Boolean> =
        elementHelper.updateElement(elementEntity)

    override fun deleteElementByList(ids: List<Int>): Observable<Boolean> =
        elementHelper.deleteElementByList(ids)

    override fun getResponseEntity(formId: Int): Observable<MutableList<ResponseEntity>> =
        responseHelper.getResponseEntity(formId)
            .flatMapIterable { it }
            .flatMap { it ->
                val response = it.copy()
                Timber.e(response.toString())
                elementHelper.getElementByList(response.elementsId)
                    .flatMapIterable {
                        Timber.e("Element Loop = ${it.toString()}")
                        it
                    }
                    .flatMap { el ->
                        Observable.fromIterable(response.elements)
                            .filter { el2 ->
                                el.elementId == el2.elementId
                            }.map {
                                val element = it.copy()
                                element.title = el.title
                                Timber.e("Element = ${element.toString()}")
                                element
                            }
                    }
                    .toList()
                    .toObservable()
                    .map {
                        Timber.e("IT Element = %s", it.toString())
                        response.elements.clear()
                        response.elements.addAll(it)
                        Timber.e("Last = %s", response.toString())
                        response
                    }
            }
            .toList()
            .toObservable()

    override fun getResponseByDesc(formId: Int): Observable<ResponseEntity> =
        responseHelper.getResponseByDesc(formId)

    override fun getResponseById(id: Int): Observable<ResponseEntity> =
        responseHelper.getResponseById(id)

    override fun deleteResponse(responseEntity: ResponseEntity): Observable<Boolean> =
        responseHelper.deleteResponse(responseEntity)

    override fun deleteResponseByList(ids: List<Int>): Observable<Boolean> =
        responseHelper.deleteResponseByList(ids)
}