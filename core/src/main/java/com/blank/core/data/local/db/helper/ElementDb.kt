package com.blank.core.data.local.db.helper

import androidx.lifecycle.LiveData
import com.blank.models.local.entity.ElementEntity
import io.reactivex.rxjava3.core.Observable

interface ElementHelper {
    fun insertElement(elementEntity: ElementEntity): Observable<Boolean>
    fun getAllElementsByIdForm(id: Int): Observable<MutableList<ElementEntity>>
    fun getElementByList(ids: List<Int>): Observable<MutableList<ElementEntity>>
    fun delete(elementEntity: ElementEntity): Observable<Boolean>
    fun getAllElementsLive(id: Int): LiveData<MutableList<ElementEntity>>
    fun updateElement(elementEntity: ElementEntity): Observable<Boolean>
    fun deleteElementByList(ids: List<Int>): Observable<Boolean>
}