package com.blank.core.data.local.db.helper

import androidx.lifecycle.LiveData
import com.blank.models.local.entity.FormEntity
import com.blank.models.local.entity.FormRelation
import io.reactivex.rxjava3.core.Observable

interface FormHelper {
    fun insertForm(form: FormEntity): Observable<Boolean>
    fun deleteForm(form: FormEntity): Observable<Boolean>
    fun getAllForm(): Observable<MutableList<FormRelation>>
    fun getAllFormLive(): LiveData<MutableList<FormEntity>>
    fun getAllFormRelationLive(): LiveData<MutableList<FormRelation>>
    fun getFormByTitle(title: String): Observable<FormEntity>
    fun updateTitleForm(title: String, newTitle: String): Observable<Boolean>
}