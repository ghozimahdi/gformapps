package com.blank.core.data

import com.blank.core.data.local.db.AppDbHelper
import com.blank.core.data.local.db.helper.ElementHelper
import com.blank.core.data.local.db.helper.FormHelper
import com.blank.core.data.local.db.helper.ResponseHelper
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by GhoziMahdi on 14/10/2019.
 * Email : -
 * No Tpln : -
 * Profesi : Mobile Engineer Development
 */
@Singleton
class AppDataManager @Inject constructor(
    formHelper: FormHelper,
    elementHelper: ElementHelper,
    responseHelper: ResponseHelper
) : DataManager, AppDbHelper(formHelper, elementHelper, responseHelper)