package com.blank.core.data.local.db

import com.blank.core.data.local.db.helper.ElementHelper
import com.blank.core.data.local.db.helper.FormHelper
import com.blank.core.data.local.db.helper.ResponseHelper
import com.blank.models.local.entity.FormRelation
import io.reactivex.rxjava3.core.Observable

interface DbHelper : ElementHelper, ResponseHelper, FormHelper {
    fun deleteForm(form: FormRelation): Observable<Boolean>
}