package com.blank.core.data.local.db.helper

import com.blank.core.data.local.db.AppDatabase
import com.blank.core.utils.rx.observableIo
import com.blank.models.local.entity.ResponseEntity
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ResponseDbHelper @Inject constructor(private val appDb: AppDatabase) : ResponseHelper {
    override fun insertResponse(responseEntity: ResponseEntity): Observable<Boolean> =
        Observable.fromCallable {
            appDb.responseDao().insert(responseEntity)
            true
        }.observableIo()

    override fun updateResponse(responseEntity: ResponseEntity): Observable<Boolean> =
        Observable.fromCallable {
            appDb.responseDao().update(responseEntity)
            true
        }.observableIo()

    override fun getResponseEntity(formId: Int): Observable<MutableList<ResponseEntity>> =
        Observable.fromCallable {
            appDb.responseDao().getResponseEntity(formId)
        }

    override fun getResponseByDesc(formId: Int): Observable<ResponseEntity> =
        Observable.fromCallable {
            appDb.responseDao().getResponseByDesc(formId)
        }

    override fun getResponseById(id: Int): Observable<ResponseEntity> =
        Observable.fromCallable {
            appDb.responseDao().getResponseById(id)
        }

    override fun deleteResponse(responseEntity: ResponseEntity): Observable<Boolean> =
        Observable.fromCallable {
            appDb.responseDao().delete(responseEntity)
            true
        }.observableIo()

    override fun deleteResponseByList(ids: List<Int>): Observable<Boolean> =
        Observable.fromCallable {
            appDb.responseDao().deleteResponseByList(ids)
            true
        }.observableIo()
}