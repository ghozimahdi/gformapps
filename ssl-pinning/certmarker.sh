#!/bin/bash
# mendapatkan certificate untuk public key pinning
# cara menggunakannya diterminal
# ./certmarker.sh api.themoviedb.org
# kemudian kopas/simpan file certificate ke dalam res -> raw -> mycertificate.crt
# kemudian gunakan class SSLCertificateConfigurator.kt dan ganti nama certificate dengan yg diatas
# langkah terakhir set config sslSocketFactory pada OkHttpClient.Builder()

$ echo | openssl s_client -servername $1 -connect $1:443 |\
  sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > mycertificate.crt