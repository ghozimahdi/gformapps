package com.blank.security.encrypt

import android.util.Base64
import timber.log.Timber
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class AESEncryption() {
    private var secretKey: SecretKey
    private var secureRandom: SecureRandom

    init {
        val iv = ByteArray(16)
        secureRandom = SecureRandom()
        secureRandom.nextBytes(iv)

        val keyGenerator = KeyGenerator.getInstance("AES")
        keyGenerator.init(256)
        secretKey = keyGenerator.generateKey()
        Timber.tag("Encryption").d("Random Key : ${secretKey.encoded}")
    }

    fun encrypt(data: String): String {
        val cipher: Cipher = Cipher.getInstance("AES")
        val secretKeySpec = SecretKeySpec(secretKey.encoded, "AES")
        val ivSpec = IvParameterSpec(secureRandom.generateSeed(16))
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivSpec)
        val cipherText: ByteArray = cipher.doFinal(data.toByteArray())
        return Base64.encodeToString(cipherText, Base64.DEFAULT)
    }

    fun decrypt(encryptedText: String): String {
        val cipherText = Base64.decode(encryptedText, Base64.DEFAULT)
        val cipher: Cipher = Cipher.getInstance("AES")
        val secretKeySpec = SecretKeySpec(secretKey.encoded, "AES")
        val ivSpec = IvParameterSpec(secureRandom.generateSeed(16))
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivSpec)
        return String(cipher.doFinal(cipherText))
    }
}