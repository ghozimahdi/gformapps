package com.blank.models.local.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "Elements")
data class ElementEntity(
    val color: Int,
    val dataType: String,
    val seqNo: String,
    var title: String,
    val description: String,
    var content: String,
    val formEntityId: Int,
    @PrimaryKey(autoGenerate = true)
    val elementId: Int = 0
) : Parcelable