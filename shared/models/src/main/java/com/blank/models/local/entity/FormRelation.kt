package com.blank.models.local.entity

import androidx.room.Embedded
import androidx.room.Relation

data class FormRelation(
    @Embedded val form: FormEntity,
    @Relation(
        parentColumn = "formId",
        entityColumn = "formEntityId"
    )
    val responses: List<ResponseEntity>,
    @Relation(
        parentColumn = "formId",
        entityColumn = "formEntityId"
    )
    val elements: List<ElementEntity>
)