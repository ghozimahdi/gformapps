package com.blank.models.local

enum class DataType(val type: String) {
    TEXT("String"), NUM("Integer"), DATE("Date"), PICTURE("Picture")
}