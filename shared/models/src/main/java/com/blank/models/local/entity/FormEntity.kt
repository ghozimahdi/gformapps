package com.blank.models.local.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "Forms")
data class FormEntity(
    val timestamp: Long,
    val elementCount: Int,
    val numOfResponse: Int,
    var title: String,
    @PrimaryKey(autoGenerate = true)
    val formId: Int = 0
) : Parcelable