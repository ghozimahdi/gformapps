package com.blank.models.local.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "Responses")
data class ResponseEntity(
    @ColumnInfo(name = "formEntityId")
    val formEntityId: Int,
    val elementsId: MutableList<Int>,
    val elements: MutableList<ElementEntity> = mutableListOf(),
    @PrimaryKey(autoGenerate = true)
    val responsesId: Int = 0
) : Parcelable