package com.blank.widget.gojek

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.Nullable
import com.blank.widget.utils.dpToPx
import com.google.android.material.card.MaterialCardView
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.ShapeAppearanceModel


class CircleCardView : MaterialCardView {
    private var shape: ShapeAppearanceModel? = null
    private var myRadius = 48
    private var position = 100.0f

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        myRadius = context dpToPx myRadius

        if (shape == null)
            shape = ShapeAppearanceModel().toBuilder()
                .setTopLeftCorner(CornerFamily.ROUNDED, myRadius.toFloat())
                .setTopRightCorner(CornerFamily.ROUNDED, myRadius.toFloat())
                .setBottomLeftCorner(CornerFamily.ROUNDED, myRadius.toFloat())
                .setBottomRightCorner(CornerFamily.ROUNDED, myRadius.toFloat())
                .build()

        shapeAppearanceModel = shape as ShapeAppearanceModel
    }

    fun changeRadius(radius: Float) {
        val r = position - radius
        val bottomRadius = if (r > myRadius.toFloat()) myRadius.toFloat() else r

        shape = ShapeAppearanceModel().toBuilder()
            .setTopLeftCorner(CornerFamily.ROUNDED, myRadius.toFloat())
            .setTopRightCorner(CornerFamily.ROUNDED, myRadius.toFloat())
            .setBottomLeftCorner(CornerFamily.ROUNDED, bottomRadius)
            .setBottomRightCorner(CornerFamily.ROUNDED, bottomRadius)
            .build()
        shapeAppearanceModel = shape as ShapeAppearanceModel
        invalidate()
    }
}