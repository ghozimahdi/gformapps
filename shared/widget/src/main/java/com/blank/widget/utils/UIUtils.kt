package com.blank.widget.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Build
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.annotation.NonNull
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.Group
import androidx.core.app.ActivityCompat
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.core.widget.ImageViewCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import java.net.URISyntaxException
import kotlin.math.roundToInt

/**
 * Created by knalb on 10/06/19.
 * Email : profghozimahdi@gmail.com
 * No Tpln : 0857-4124-4919
 * Profesi : Mobile Development Engineer
 */

fun MenuItem.show() {
    isVisible = true
}

fun MenuItem.hide() {
    isVisible = false
}

fun View.hide() {
    visibility = View.GONE
}

fun View.show() {
    visibility = View.VISIBLE
}

fun getRoundedImageTarget(
    context: Context,
    imageView: ImageView,
    radius: Float
): BitmapImageViewTarget =
    object : BitmapImageViewTarget(imageView) {
        override fun setResource(resource: Bitmap?) {
            val circularBitmapDrawable =
                RoundedBitmapDrawableFactory.create(context.resources, resource)
            circularBitmapDrawable.cornerRadius = radius
            imageView.setImageDrawable(circularBitmapDrawable)
        }
    }

fun TabLayout.setupWithViewPager(viewPager2: ViewPager2, labels: Array<String>) {
    TabLayoutMediator(this, viewPager2) { tab, position ->
        tab.text = labels[position]
    }.attach()
}

fun <T> LifecycleOwner.observe(liveData: LiveData<T>, action: (T) -> Unit) {
    liveData.observe(this, Observer(action))
}

fun AppCompatImageView.setImageRounded(url: String, radius: Float) {
    val requestOptions = RequestOptions().optionalTransform(RoundedCorners(9))

    Glide.with(this)
        .asBitmap()
        .load(url)
        .apply(requestOptions)
        .into(getRoundedImageTarget(context, this, radius))
}

fun ImageView.setImages(url: String) {
    Glide.with(this)
        .load(url)
        .into(this)
}

fun FloatingActionButton.changeColor(click: Boolean) {
    if (click) {
        ImageViewCompat.setImageTintList(
            this,
            ColorStateList.valueOf(Color.RED)
        )
    } else {
        ImageViewCompat.setImageTintList(
            this,
            ColorStateList.valueOf(Color.WHITE)
        )
    }
}

infix fun View.goTo(cls: Class<*>) {
    val i = Intent(rootView.context, cls)
    setOnClickListener { rootView.context.startActivity(i) }
}

infix fun Context.goTo(url: String) {
    val i = Intent.getIntentOld(url)
    startActivity(i)
}

infix fun Context.goTo(cls: Class<*>) {
    startActivity(Intent(this, cls))
}


infix fun View.goTo(url: String) {
    try {
        val i = Intent.getIntentOld(url)
        setOnClickListener { rootView.context.startActivity(i) }
    } catch (e: URISyntaxException) {
        e.printStackTrace()
    }
}

internal fun Activity.screenWidth(): Int {
    val displayMetrics = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.widthPixels
}

infix fun Context.dpToPx(dp: Int): Int {
    val r = resources
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), r?.displayMetrics)
        .roundToInt()
}

infix fun Context.dip2px(dpValue: Float): Int {
    val scale = resources.displayMetrics.density
    return (dpValue * scale + 0.5f).toInt()
}

infix fun Context.px2dip(pxValue: Float): Int {
    val scale = this.resources.displayMetrics.density
    return (pxValue / scale + 0.5f).toInt()
}

internal fun Context.showKeyboard() {
    val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
}

internal fun Activity.hideKeyboard() {
    val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    var view = this.currentFocus
    if (view == null) {
        view = View(this)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

infix fun Context.requestOption(radius: Int): RequestOptions =
    RequestOptions().transform(CenterCrop(), RoundedCorners(radius))

internal fun Window.transparentStatusBar() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        this.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
    }

}

internal fun Window.hideStatusBar() {
    this.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
}

internal fun Window.showStatusBar() {
    this.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
}

internal fun Activity.requestPermission(@NonNull permissions: Array<String>, requestCode: Int) {
    try {
        ActivityCompat.requestPermissions(this, permissions, requestCode)
    } catch (e: URISyntaxException) {
        e.printStackTrace()
    }
}

internal fun Activity.checkPermission(@NonNull permissions: String): Boolean = try {
    ActivityCompat.checkSelfPermission(
        this,
        permissions
    ) == PackageManager.PERMISSION_GRANTED
} catch (e: URISyntaxException) {
    e.printStackTrace()
    false
}

fun Group.setAlphaForAll(alpha: Float) = referencedIds.forEach {
    rootView.findViewById<View>(it).alpha = alpha
}