package com.blank.widget.gojek

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.Nullable
import com.blank.widget.R
import com.blank.widget.utils.dpToPx


class IconImageView : LinearLayout {
    private var bgId: Int? = null
    private var imageId: Int? = null
    private var title: String? = null

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.IconImageView, 0, 0)
        try {
            title = a.getString(R.styleable.IconImageView_titleText)
            bgId = a.getResourceId(R.styleable.IconImageView_backgroundImage, 0)
            imageId = a.getResourceId(R.styleable.IconImageView_setImage, 0)

            val imageView = ImageView(context)
            val imageParams = LayoutParams(context dpToPx 40, context dpToPx (40))
            imageView.layoutParams = imageParams
            imageView.setPadding(
                context dpToPx (8),
                context dpToPx (8),
                context dpToPx (8),
                context dpToPx (8)
            )
            imageView.requestLayout()

            imageId?.let { imageView.setImageResource(it) }
            bgId?.let { imageView.setBackgroundResource(it) }

            val txt = TextView(context)
            val txtParams = LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            txtParams.setMargins(
                0,
                context dpToPx (5),
                0,
                0
            )
            txt.layoutParams = txtParams
            txt.text = title.toString()
            txt.requestLayout()

            orientation = VERTICAL
            gravity = Gravity.CENTER_HORIZONTAL

            addView(imageView)
            addView(txt)
        } finally {
            a.recycle()
        }
    }
}